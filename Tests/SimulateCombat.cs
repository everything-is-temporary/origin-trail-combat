using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using CombatSim.CoreSim.Effects;
using CombatSim.CoreSim.Stats;
using CombatSim.Utility.Enumerations;

namespace CombatSim.CoreSim
{
    public class SimulateCombat
    {
        static void Main(string[] args)
        {
            DebugPrint3();
        }

        private static void DebugPrint1()
        {
            System.Console.WriteLine("Hello");
            CombatStats<float> g = new CombatStats<float>();
            System.Console.WriteLine(g);
            TargetType t = TargetType.Ally;
            var z = TargetType.Self | TargetType.Ally;
            Console.WriteLine(z == TargetType.Ally);
            CombatState s = new CombatState();
            Console.WriteLine(s);
            TargetType tt = new TargetType(TargetType.Ally | TargetType.Self);
            Console.WriteLine(tt);
        }

        private static void DebugPrint2()
        {
            CombatEffect ce = new TestCombatEffect();
            CombatItem ci = new CombatItem();
            ci.Effect.Add(ce);
            CombatActor ca = new CombatActor();
            ca.Items.Add(ci);
            CombatState cs = new CombatState();
            cs.AddActor(ca);
            Console.WriteLine(cs);
        }

        private static void DebugPrint3()
        {
            CombatGame cg = new CombatGame();
            var initialState = cg.GetInitialState(GetMockCombatActorList());
//            Console.WriteLine(initialState);
//            var h = cg.GetValidTargets(initialState, initialState.ActiveCombatActor, TargetType.Enemy);
//            Console.WriteLine(string.Join(", ", h));
            Console.WriteLine(cg.RunGame(initialState));
            
        }

        private static void DebugPrint4()
        {
            CombatStats <int> e = GetMockItem().StatsActual;
            CombatStats <int> f = GetMockItem().StatsActual;
            f[CombatStatType.Health] = 0;
            Console.WriteLine(e-f);

        }

        private static List<CombatActor> GetMockCombatActorList()
        {
            return new List<CombatActor> {GetMockPlayer(), GetMockEnemy()} ;
        }

        #region Mocks

        private static int rngSeed = 3;
        private static int maxStat = 200;
        private static int maxHealth = 500;
        
        private static CombatActor GetMockPlayer()
        {
            CombatActor player = new CombatActor(0);
            player.Items.Add(GetMockItem());
            player.CanReposition = false;
            player.StatsActual = GetRandomStats();
            foreach (CombatStatType statType in player.StatsMaximum.Keys.ToList())
            {
                player.StatsMaximum[statType] = maxStat;
            }

            player.StatsActual[CombatStatType.Health] = player.StatsMaximum[CombatStatType.Health] = maxHealth;
            player.Controller = new TestRandomItemSelectionController();
            return player;
        }

        private static CombatActor GetMockEnemy()
        {
            CombatActor enemy = new CombatActor(1);
            enemy.Items.Add(GetMockItem());
            enemy.StatsActual = GetRandomStats();
            foreach (CombatStatType statType in enemy.StatsMaximum.Keys.ToList())
            {
                enemy.StatsMaximum[statType] = maxStat;
            }

            enemy.StatsActual[CombatStatType.Health] = enemy.StatsMaximum[CombatStatType.Health] = maxHealth;
            enemy.Controller = new TestRandomItemSelectionController();
            return enemy;
        }

        private static CombatItem GetMockItem()
        {
            CombatItem ci = new CombatItem();
            ci.StatsActual = GetRandomStats();
            ci.NumTargets = 1;
            ci.Quantity = 2;
            return ci;
        }

        
        private static CombatStats<int> GetRandomStats()
        {
            Random rng = new Random(rngSeed);
            CombatStats<int> rngStats = new CombatStats<int>();            
            foreach (CombatStatType statType in rngStats.Keys.ToList())
            {
                rngStats[statType] = (int)Math.Floor(rng.NextDouble() * maxStat);
            }

            return rngStats;
        }
        
        #endregion
        
    }
}