using System;
using System.Data;
using System.Linq;
using CombatSim.CoreSim.Controller;

namespace CombatSim.CoreSim
{
    public class TestRandomItemSelectionController : IActorController
    {

        private static Random rng = new Random();
        public ActionCommand MakeMove(CombatGame combatGame, CombatState combatState)
        {
            var itemMoves = combatGame.GetAllMoves(combatState)
                .Where(command => command.ActionCommandType == ActionCommandType.Item).ToList();
            if (itemMoves.Count == 0)
            {
                return ActionCommand.Flee;
            }
            var itemMove = itemMoves[rng.Next(itemMoves.Count)];
            var targets = combatGame.GetValidTargets(combatState, combatState.ActiveCombatActor, itemMove.CombatItem);
            itemMove.AddTarget(targets[rng.Next(targets.Count)]);
            return itemMove;
        }
    }
}