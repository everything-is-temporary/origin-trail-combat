using System.Collections.Generic;
using System.Linq;
using CombatSim.CoreSim.Effects;

namespace CombatSim.CoreSim
{
    /// <summary>
    /// Test Combat Effect. Reverses Turn Order.
    /// </summary>
    public class TestCombatEffect : CombatEffect
    {
        public override void Apply(CombatState combatStatem, CombatActor affectedActor)
        {
            affectedActor.StatsMultiplier[CombatStatType.Speed] *= 0.5f;
        }
    }
}