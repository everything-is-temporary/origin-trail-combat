namespace CombatSim.Utility.Interfaces
{
    public interface IDiffable<T>
    {
        T Copy();
        T Diff(T other);
    }
}