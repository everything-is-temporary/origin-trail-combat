namespace CombatSim.Utility.Enumerations
{
    public class StateTimingType: BitflagEnumeration
    {
        public static readonly StateTimingType Never = new StateTimingType(0b1);
        public static readonly StateTimingType GameTurnStart = new StateTimingType(0b10);
        public static readonly StateTimingType GameTurnEnd = new StateTimingType(0b100);
        public static readonly StateTimingType CharacterTurnStart = new StateTimingType(0b1000);
        public static readonly StateTimingType CharacterTurnEnd = new StateTimingType(0b10000);
        public static readonly StateTimingType ActionStart = new StateTimingType(0b100000);

        public StateTimingType(int bitValue) : base(bitValue)
        {
        }
        
        public StateTimingType(BitflagEnumeration enumeration) : base(enumeration)
        {
        }
    }
}