using System;
using System.Collections.Generic;
using System.Text;
using CombatSim.CoreSim.Effects;
using CombatSim.CoreSim.Stats;

namespace CombatSim.CoreSim
{
    public class CombatItem
    {
        public int Quantity = 1;

        public CombatStats<int> StatsActual = new CombatStats<int>();

        public TargetType TargetType = TargetType.Enemy;

        public int NumTargets = 1;
        
        public List<CombatEffect> Effect { get; protected set; } = new List<CombatEffect>();

        public override string ToString()
        {
           StringBuilder sb = new StringBuilder();
           sb.Append("\n{");
           sb.Append($"\n\t{GetHashCode()} Quantity: {Quantity}, Targets: {NumTargets}, TargetType: {TargetType}");
           sb.Append(StatsActual.ToString().Replace("\n", "\n\t"));
           sb.Append("\n}");
           return sb.ToString();
        }
    }
}