using System;
using System.Collections.Generic;
using System.Text;
using CombatSim.CoreSim.Controller;
using CombatSim.CoreSim.Effects;
using CombatSim.CoreSim.History;
using CombatSim.CoreSim.Stats;
using CombatSim.Utility;
using CombatSim.Utility.Enumerations;

namespace CombatSim.CoreSim
{
    public class CombatActor
    {

        public IActorController Controller;
        
        public string Name = Guid.NewGuid().ToString(); // Temporarily here to uniquely identify actors
        public int GroupId = 0;

        public CombatStats<int> StatsMinimum = new CombatStats<int>();
        public CombatStats<int> StatsMaximum = new CombatStats<int>();
        public CombatStats<int> StatsActual = new CombatStats<int>();
        public CombatStats<float> StatsMultiplier = new CombatStats<float>(1.0f);
        public TargetType CanTarget = TargetType.Everybody;
        public TargetType CanBeTargetedBy = TargetType.Everybody;
        
        public HashSet<CombatItem> Items = new HashSet<CombatItem>();

        public bool CanReposition = true;
        public double Position = 0;

        public bool CanFight => StatsActual[CombatStatType.Health] > 0;

        /// <summary>
        /// Returns the kind of target <param name="other"></param> is for this Actor.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>TargetType respresenting the type of target "other" is to this Actor.</returns>
        public TargetType TargetFor(CombatActor other)
        {
            // TODO: Find if we have a better signifier (can't use reference due to copy changing references)
            if (other.Name == Name)
            {
                return TargetType.Self;
            }

            return other.GroupId == GroupId ? TargetType.Ally : TargetType.Enemy;
        }
        
        public int GetStat(CombatStatType statType)
        {
            if (!StatsActual.ContainsKey(statType)) return 0; // Shiuld never be the case, stat objects are auto-initialized
            if (StatsMultiplier.TryGetValue(statType, out var multiplier))
            {
                return (int) Math.Round(multiplier * StatsActual[statType]);
            }
            else
            {
                return StatsActual[statType];
            }
        }
        
        public CombatActor()
        {
            
        }
        
        public CombatActor(int groupId)
        {
            GroupId = groupId;
        }

        public CombatActor(CombatActor combatActor)
        {
            Name = combatActor.Name;
            GroupId = combatActor.GroupId;
            StatsMinimum = combatActor.StatsMinimum.Copy();
            StatsMaximum = combatActor.StatsMaximum.Copy();
            StatsActual = combatActor.StatsActual.Copy();
            StatsMultiplier = combatActor.StatsMultiplier.Copy();
            CanTarget = new TargetType(combatActor.CanTarget);
            CanBeTargetedBy = new TargetType(combatActor.CanBeTargetedBy);
            CanReposition = combatActor.CanReposition;
            Position = combatActor.Position;
        }
        
        public CombatActor Copy()
        {
            return new CombatActor();
        }

        public void ApplyAll(StateTimingType timingType, CombatState state)
        {
            ApplyAllEffects(timingType, state);
            ApplyAllCountdowns(timingType, state);
        }
        
        public void ApplyAllEffects(StateTimingType timingType, CombatState combatState)
        {
            if(effectsByApplication.TryGetValue(timingType, out EffectQueue effectQueue))
            {
                effectQueue.ApplyAllEffects(combatState, this);
            }
        }
        
        public void ApplyAllCountdowns(StateTimingType timingType, CombatState state)
        {
            if(effectsByCountdown.TryGetValue(timingType, out EffectQueue effectQueue))
            {
                effectQueue.ApplyAllCountdowns(state, this);
            }
        }

        public void AddEffect(CombatEffect combatEffect)
        {
            if (!effectsByApplication.ContainsKey(combatEffect.EffectApplicationTiming))
            {
                effectsByApplication.Add(combatEffect.EffectApplicationTiming, new EffectQueue());
            }

            if (!effectsByCountdown.ContainsKey(combatEffect.CountdownTiming))
            {
                effectsByCountdown.Add(combatEffect.CountdownTiming, new EffectQueue());
            }
            
            effectsByApplication[combatEffect.EffectApplicationTiming].Add(combatEffect);
            effectsByCountdown[combatEffect.CountdownTiming].Add(combatEffect);
        }

        public void DecrementItem(CombatItem item)
        {
            if (!Items.Contains(item))
            {
                throw new KeyNotFoundException($"Item {item} cannot be decremented since it does not exist in the items list. Ensure references are identical.");
            }

            if (--item.Quantity <= 0)
            {
                Items.Remove(item);
            }
        }

        public void TakeDamage(double damage)
        {
            // TODO: Refactor stats to doubles.
            StatsActual[CombatStatType.Health] = (int)MathUtils.Clamp(StatsActual[CombatStatType.Health] - damage, StatsMinimum[CombatStatType.Health],
                StatsMaximum[CombatStatType.Health]);
        }

        // Cleanup phase so fainted/ dead characters don't display animations.
        public void ExitActiveCombat()
        {
            effectsByApplication.Clear();
            effectsByCountdown.Clear();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\n{\n");
            sb.Append($"\t{Name}\n");
            sb.Append("\t=== Stats ===\n");
            // TODO: Modify Stats object to be indexable and contain dictionary access methods.
            foreach (CombatStatType statType in StatsActual.Stats.Keys)
            {
                // It is OK for floats to not be completely equal here
                string multiplierString = StatsMultiplier.Stats[statType] == 1.0f
                    ? $" x{StatsMultiplier.Stats[statType]}"
                    : "";
                sb.Append($"\t\t[{statType}]: {StatsActual.Stats[statType]}{multiplierString}, Range({StatsMinimum.Stats[statType]}, {StatsMaximum.Stats[statType]})\n");
            }
            sb.Append("\t=============\n");
            if (CanTarget != TargetType.Everybody)
            {
                sb.Append($"Can Target: {CanTarget}\n");
            }

            if (CanBeTargetedBy != TargetType.Everybody)
            {
                sb.Append($"\tCan be targeted by: {CanBeTargetedBy}\n");
            }
            sb.Append("\t=== Items ===\n");
            foreach (CombatItem combatItem in Items)
            {
                sb.Append($"\t\t{combatItem.ToString().Replace("\n", "\n\t\t")}\n");
            }
            sb.Append("\t=============\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        private Dictionary<StateTimingType, EffectQueue> effectsByApplication = new Dictionary<StateTimingType, EffectQueue>();
        private Dictionary<StateTimingType, EffectQueue> effectsByCountdown = new Dictionary<StateTimingType, EffectQueue>();


    }
}