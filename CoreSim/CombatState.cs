using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using CombatSim.CoreSim.History;
using CombatSim.Utility.Interfaces;

namespace CombatSim.CoreSim
{
    public class CombatState : IDiffable<CombatState>
    {

        public static readonly CombatState Empty = new CombatState();
        
        public int TurnNumber = 0;
        
        public CombatActor ActiveCombatActor => TurnOrder.Count > 0 ? Actors[TurnOrder.Peek()] : null;

        public List<CombatActor> AllActors => Actors;
        
        public List<CombatActor> GetActorsInActiveCombat => Actors.Where(actor => actor.CanFight).ToList();

        public void AddActor(CombatActor combatActor)
        {
            Actors.Add(combatActor);
            indexMapping.Add(combatActor, Actors.Count-1);
        }

        public bool RemoveActor(CombatActor combatActor)
        {
            if (!indexMapping.ContainsKey(combatActor))
            {
                return false;
            }
            Actors.RemoveAt(indexMapping[combatActor]);
            return indexMapping.Remove(combatActor);
        }

        public CombatActor GetActorFromIndex(int actorIndex)
        {
            return Actors[actorIndex];
        }
        
        public int GetIndexFromActor(CombatActor combatActor)
        {
            return indexMapping[combatActor];
        }
        
        public Queue<int> TurnOrder = new Queue<int>();

        public List<CombatEvent> History { get; protected set; } = new List<CombatEvent>();
        
        public CombatState()
        {
        }

        public CombatState(CombatState combatState)
        {
            this.TurnNumber = combatState.TurnNumber;
            foreach (CombatActor combatActor in combatState.Actors)
            {
                AddActor(combatActor);
            }
            this.TurnOrder = new Queue<int>(combatState.TurnOrder);
            // OK to shallow copy this, values not intended to be modifiable.
            this.History = new List<CombatEvent>(combatState.History);
        }

        /// <summary>
        /// Returns the difference between 2 combat states as a new combat state.
        /// </summary>
        /// <param name="other">The old Combat State</param>
        /// <returns>The elements in this state that are not in the other state.</returns>
        public CombatState Diff(CombatState other)
        {
            CombatState diff = new CombatState();
            throw new NotImplementedException();
        }

        public List<CombatEvent> GetEvents(CombatState other)
        {
            throw new NotImplementedException();
        }

        public void AppendEvents(CombatState other, ref List<CombatEvent> combatEvents)
        {
            combatEvents.AddRange(GetEvents(other));
        }
        
        public CombatState Copy()
        {
            return new CombatState(this);
        }
        
        public static List<CombatEvent> operator -(CombatState newState, CombatState oldState)
        {
            return newState.History.Except(oldState.History).ToList();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("=========================\n");
            sb.Append($"--- Turn Number ---\n\t{TurnNumber}\n");
            sb.Append($"--- Turn Order ---\n\t[{string.Join(", ", TurnOrder.ToArray())}]\n");
            if (TurnOrder.Count > 0)
            {
                int activePlayerIndex = TurnOrder.Peek();
                sb.Append($"- Active Player -\n\t{activePlayerIndex}: {Actors[activePlayerIndex].Name}\n");
            }
            sb.Append("--- Actors ---\n");
            for (int i = 0; i < Actors.Count; i++)
            {
                sb.Append($"\t--- Actor {i} ---");
                sb.Append(Actors[i].ToString().Replace("\n", "\n\t\t"));
            }
            sb.Append("\n=========================\n");
            return sb.ToString();
        }
        
        private List<CombatActor> Actors = new List<CombatActor>();

        private Dictionary<CombatActor, int> indexMapping = new Dictionary<CombatActor, int>();
        
    }
}