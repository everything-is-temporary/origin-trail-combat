namespace CombatSim.CoreSim.Effects
{
    /// <summary>
    /// Effect Duration, conveyed in CombatEvents to trigger animations.
    /// </summary>
    public enum EffectDurationType
    {
        Persistent, // Occurs until a stop signal or End of Battle, whichever occurs first.
        Momentary // Occurs Immediately, before the state is changed
    }
}