using System.Collections.Generic;
using CombatSim.CoreSim.History;
using System.Linq;
using CombatSim.Utility.Enumerations;

namespace CombatSim.CoreSim.Effects
{
    public class EffectQueue
    {
        /// <summary>
        /// Recreates the internal queue objects in preparation for applying effects sequentially.
        /// </summary>
        /// <returns>The count of elements in the Queue.</returns>
        public int SetUp()
        {
            _internalQueue.Clear();
            foreach (CombatEffect combatEffect in _internalEffects.ToList().OrderBy(effect => effect))
            {
                _internalQueue.Enqueue(combatEffect);
            }

            return _internalQueue.Count;
        }
        
        public bool CanApply => _internalQueue.Count > 0;

        public void Add(CombatEffect combatEffect)
        {
            _internalEffects.Add(combatEffect);
            // TODO: Add a Priority Queue and insert at correct priority
            _internalQueue.Enqueue(combatEffect);
        }

        
//        public bool TryApplyNext(CombatState state, out CombatState resultingState)
//        {
//            if (_internalQueue.TryDequeue(out CombatEffect result))
//            {   
//                resultingState = result.Apply(state);
//                return true;
//            }
//
//            resultingState = state;
//            return false;
//        }
//        
//        public CombatState ApplyNext(CombatState state)
//        {
//            TryApplyNext(state, out CombatState resultingState);
//            return resultingState;
//        }

//        // Commented out since now the resulting list of combatEvents is redundant.
//        public bool TryApplyNext(CombatState state, out CombatState resultingState, out List<CombatEvent> resultingEvents)
//        {
//            if (TryApplyNext(state, out resultingState))
//            {
//                resultingEvents = (resultingState - state);
//                return true;
//            }
//
//            resultingEvents = new List<CombatEvent>();
//            return false;
//        }
//
//        public CombatState ApplyNext(CombatState state, out List<CombatEvent> combatEvents)
//        {
//            TryApplyNext(state, out CombatState resultingState, out combatEvents);
//            return resultingState;
//        }
//        
//        public CombatState ApplyAll(CombatState state, ref List<CombatEvent> combatEventList)
//        {
//            while (TryApplyNext(state, out CombatState resultingState, out List<CombatEvent> resultingEventList))
//            {
//                // I was worried about potentially overriding state before it is used to calculate the
//                // event if "state" was an out parameter on the above line.
//                state = resultingState;
//                combatEventList.AddRange(resultingEventList);
//            }
//
//            return state;
//        }
//
//        // No checks, just apply all.
//        public CombatState ApplyAllEffects(CombatState state)
//        {
//            while (TryApplyNext(state, out CombatState resultingState))
//            {
//                state = resultingState;
//            }
//
//            return state;
//        }


        public void ApplyEffect(CombatState state, CombatEffect effect, CombatActor affectedActor)
        {
            effect.Apply(state, affectedActor);
            state.History.Add(new CombatEvent(CombatEventType.PersistentEffectApplied, effect));
        }

        public void ApplyAllEffects(CombatState state, CombatActor affectedActor)
        {
            SetUp();
            while (_internalQueue.Count > 0)
            {
                ApplyEffect(state, _internalQueue.Dequeue(), affectedActor);
            }
        }

        public void ApplyCountdown(CombatState state, CombatEffect effect, CombatActor affectedActor)
        {
            if (--effect.Countdown <= 0)
            {
                effect.Remove(state, affectedActor);
                state.History.Add(new CombatEvent(CombatEventType.PersistentEffectEnded, effect));
                _internalEffects.Remove(effect);
            }
        }

        // No checks, just apply all.
        public void ApplyAllCountdowns(CombatState state, CombatActor affectedActor)
        {
            foreach (CombatEffect combatEffect in _internalEffects)
            {
                ApplyCountdown(state, combatEffect, affectedActor);
            }
        }
        
        public void ApplyAllCountdowns(CombatState state, StateTimingType timingType, CombatActor affectedActor)
        {
            foreach (CombatEffect combatEffect in _internalEffects)
            {
                if (combatEffect.CountdownTiming.ContainsAll(timingType))
                {
                    ApplyCountdown(state, combatEffect, affectedActor);
                }
            }
        }
        
        private Queue<CombatEffect> _internalQueue = new Queue<CombatEffect>();
        private HashSet<CombatEffect> _internalEffects = new HashSet<CombatEffect>();
    }
}