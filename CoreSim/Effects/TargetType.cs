using System.Collections.Generic;
using System.Linq;
using System.Text;
using CombatSim.Utility.Enumerations;

namespace CombatSim.CoreSim.Effects
{
    /// <summary>
    ///
    /// Using typesafe Enum Pattern: https://www.infoworld.com/article/3198453/how-to-implement-a-type-safe-enum-pattern-in-c.html
    /// </summary>
    public class TargetType : BitflagEnumeration
    {
        public static readonly TargetType Nobody = new TargetType(0b0);
        public static readonly TargetType Self = new TargetType(0b1);
        public static readonly TargetType Ally = new TargetType(0b10);
        public static readonly TargetType Enemy = new TargetType(0b100);
        public static readonly TargetType Everybody = new TargetType(~Nobody.Value);

        public TargetType(int bitValue) : base(bitValue)
        {
        }

        public TargetType(BitflagEnumeration enumeration) : base(enumeration)
        {
        }

        public override string ToString()
        {
            List<string> possibleTargets = new List<string>();            
            if (ContainsAll(Self))
            {
                possibleTargets.Add(nameof(Self));
            }

            if (ContainsAll(Ally))
            {
                possibleTargets.Add(nameof(Ally));
            }

            if (ContainsAll(Enemy))
            {
                possibleTargets.Add(nameof(Enemy));
            }

            return $"[{string.Join(", ", possibleTargets)}]";
        }
    }
}