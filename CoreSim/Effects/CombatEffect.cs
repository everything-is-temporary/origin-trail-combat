using System;
using System.Text;
using CombatSim.Utility.Enumerations;

namespace CombatSim.CoreSim.Effects
{
    public abstract class CombatEffect
    {
        public int Countdown { get; set; } = 1;

        public StateTimingType CountdownTiming { get; protected set; } = StateTimingType.CharacterTurnEnd;

        public StateTimingType EffectApplicationTiming { get; protected set; } = StateTimingType.CharacterTurnEnd;

        public EffectDurationType EffectDuration { get; protected set; } = EffectDurationType.Momentary;
        public int Priority { get; protected set; } = 0;

        public TargetType TargetType { get; protected set; } = TargetType.Everybody;

        public bool IsActive => EffectDuration == EffectDurationType.Persistent && Countdown > 0;

        public abstract void Apply(CombatState combatState, CombatActor affectedActor);

        public virtual void Remove(CombatState combatState, CombatActor affectedActor)
        {
            return;
        }

        public static bool operator >(CombatEffect first, CombatEffect second)
        {
            return first.Priority > second.Priority;
        }

        public static bool operator <(CombatEffect first, CombatEffect second)
        {
            return first.Priority < second.Priority;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append($"\tType: {GetType()}");
            sb.Append($"\tDuration: {EffectDuration}");
            if (Priority != 0)
            {
                sb.Append($"\tPriority: {Priority}");
            }
            if (EffectDuration == EffectDurationType.Persistent)
            {
                sb.Append($"\tCountdown: {Countdown}");

                sb.Append($"\tApplication Timing: {EffectApplicationTiming}");
                sb.Append($"\tCountdownTiming: {CountdownTiming}");
            }
            sb.Append($"\tEffectTargetType: {TargetType}");
            sb.Append("}");
            return base.ToString();
        }
    }
}