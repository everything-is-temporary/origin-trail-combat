using System;

namespace CombatSim.CoreSim.Effects.EffectLibrary
{
    public class HealingPercentage : CombatEffect
    {

        private double _healingPercentage;
        
        
        public HealingPercentage(double healingPercentage)
        {
            EffectDuration = EffectDurationType.Momentary;
            _healingPercentage = healingPercentage;            
        }
        
        public override void Apply(CombatState combatState, CombatActor effectTarget)
        {
            var percentageRestored = effectTarget.StatsActual[CombatStatType.Health] /
                                     effectTarget.StatsMaximum[CombatStatType.Health];
            effectTarget.StatsActual[CombatStatType.Health] = Math.Min(effectTarget.StatsMaximum[CombatStatType.Health],
                effectTarget.StatsActual[CombatStatType.Health] *= (1 + percentageRestored));
        }
    }
}