using System;
using System.Collections.Generic;

namespace CombatSim.CoreSim.History
{
    public class CombatEventType
    {
        
        #region MetaEvents

        public static readonly CombatEventType StartCombat = new CombatEventType("StartCombat");
        public static readonly CombatEventType EndCombat = new CombatEventType("EndCombat");
        public static readonly CombatEventType ChangeTurn = new CombatEventType("ChangeTurn");
        public static readonly CombatEventType ChangeTurnOrder = new CombatEventType("ChangeTurnOrder");
        public static readonly CombatEventType ActorEnterCombat = new CombatEventType("ActorEnterCombat");
        public static readonly CombatEventType ActorExitCombat = new CombatEventType("ActorExitCombat");
        
        private static readonly List<CombatEventType> MetaEvents = new List<CombatEventType>{ StartCombat, EndCombat, ChangeTurn, ChangeTurnOrder, ActorEnterCombat, ActorExitCombat };
        public static bool IsMetaEvent(CombatEventType combatEventType) => MetaEvents.Contains(combatEventType);

        #endregion

        #region ActionEvents
        
        public static readonly CombatEventType ItemUsed = new CombatEventType("ItemUsed");
        public static readonly CombatEventType NoActionMade = new CombatEventType("NoActionMade");
        public static readonly CombatEventType AttemptedFleeBattle = new CombatEventType("AttemptedFleeBattle");
        public static readonly CombatEventType DamageTaken = new CombatEventType("DamageTaken");

        private static readonly List<CombatEventType> ActionEvents = new List<CombatEventType>{ ItemUsed, NoActionMade, AttemptedFleeBattle };
        public static bool IsActionEvent(CombatEventType combatEventType) => ActionEvents.Contains(combatEventType);
        
        #endregion

        #region EffectEvents
        
        public static readonly CombatEventType MomentaryEffectApplied = new CombatEventType("MomentaryEffectApplied");
        public static readonly CombatEventType PersistentEffectApplied = new CombatEventType("PersistentEffectApplied");
        public static readonly CombatEventType PersistentEffectEnded = new CombatEventType("PersistentEffectEnded");
        public static readonly CombatEventType ActorPositionUpdated = new CombatEventType("ActorPositionUpdated");
        
        private static readonly List<CombatEventType> EffectEvents = new List<CombatEventType>{ MomentaryEffectApplied, PersistentEffectApplied, PersistentEffectEnded, ActorPositionUpdated };
        public static bool IsEffectEvent(CombatEventType combatEventType) => EffectEvents.Contains(combatEventType);

        #endregion
        
        public readonly string Value;

        public CombatEventType(string valueString)
        {
            Value = valueString;
        }

        // Not entirely sure where things go with this, the ideal result is one event per CombatEventType
        public delegate void CombatEventOccurred(object eventSource, EventArgs args);
        public event CombatEventOccurred OnCombatEvent;

        public CombatEventType Broadcast()
        {
            OnCombatEvent(this, EventArgs.Empty);
            return this; // returned so it's easy to Broadcast a "new" call.
        }

        public override string ToString()
        {
            return Value;
        }
    }
}