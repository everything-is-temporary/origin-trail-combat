using System.Collections.Generic;

namespace CombatSim.CoreSim.History
{
    public class CombatEvent
    {
        public CombatEventType CombatEventType { get; protected set; }

        public object EventData { get; protected set; }

        public CombatEvent(CombatEventType combatEventType)
        {
            CombatEventType = combatEventType;
        }
        
        public CombatEvent(CombatEventType combatEventType, object eventData)
        {
            CombatEventType = combatEventType;
            EventData = eventData;
        }
    }
}