using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;

namespace CombatSim.CoreSim.Stats
{
    public class CombatStats<T> : IDictionary<CombatStatType, T>
    {

        public Dictionary<CombatStatType, T> Stats { get; protected set; }

        public ICollection<CombatStatType> Keys => ((IDictionary<CombatStatType, T>)Stats).Keys;

        public ICollection<T> Values => ((IDictionary<CombatStatType, T>)Stats).Values;

        public int Count => ((IDictionary<CombatStatType, T>)Stats).Count;

        public bool IsReadOnly => ((IDictionary<CombatStatType, T>)Stats).IsReadOnly;

        public T this[CombatStatType key] { get => ((IDictionary<CombatStatType, T>)Stats)[key]; set => ((IDictionary<CombatStatType, T>)Stats)[key] = value; }

        /// <summary>
        /// Create. Note: Initializes all stats in existence to their default value.
        /// </summary>
        public CombatStats()
        {
            Stats = new Dictionary<CombatStatType, T>();
            foreach (CombatStatType statType in Enum.GetValues(typeof(CombatStatType)))
            {
                //!Important: auto-initialization to default value
                Stats.Add(statType, default(T));
            }
        }
        
        public CombatStats(T defaultValue)
        {
            Stats = new Dictionary<CombatStatType, T>();
            foreach (CombatStatType statType in Enum.GetValues(typeof(CombatStatType)))
            {
                // fixme Multiplier treatment
                Stats.Add(statType, defaultValue);
            }
        }

        public CombatStats(Dictionary<CombatStatType, T> existingStats)
        {
            Stats = existingStats;
        }

        public static CombatStats<T> operator -(CombatStats<T> previous, CombatStats<T> current)
        {
            //Stat object must contain identical StatType keys, never remove stats to be compared
            Debug.Assert(current.Stats.Keys.All(previous.Stats.Keys.Contains));
            CombatStats<T> diffStats = current.Copy();
            foreach (KeyValuePair<CombatStatType, T> pair in current.Stats.ToList())
            {
                // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/expression-trees/how-to-execute-expression-trees
                var cur = Expression.Constant(previous.Stats[pair.Key]);
                var prev = Expression.Constant(pair.Value);
                var f = Expression.Subtract(cur, prev);
                diffStats.Stats[pair.Key] = Expression.Lambda<Func<T>>(f).Compile()();
            }

            return diffStats;
        }

        public bool ModifyStat(CombatStatType statToModify, T newValue)
        {
            if (!Stats.ContainsKey(statToModify)) return false;
            Stats[statToModify] = newValue;
            return true;
        }

        public CombatStats<T> Copy()
        {
            return new CombatStats<T>(Stats);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\n{");
            foreach (KeyValuePair<CombatStatType, T> stat in Stats)
            {
                sb.Append($"\n\t[{Enum.GetName(typeof(CombatStatType), stat.Key)}]: \t{stat.Value}");
            }

            sb.Append("\n}");
            return sb.ToString();
        }

        #region DictionaryImplementationThroughStats

        public void Add(CombatStatType key, T value)
        {
            ((IDictionary<CombatStatType, T>)Stats).Add(key, value);
        }

        public bool ContainsKey(CombatStatType key)
        {
            return ((IDictionary<CombatStatType, T>)Stats).ContainsKey(key);
        }

        public bool Remove(CombatStatType key)
        {
            return ((IDictionary<CombatStatType, T>)Stats).Remove(key);
        }

        public bool TryGetValue(CombatStatType key, out T value)
        {
            return ((IDictionary<CombatStatType, T>)Stats).TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<CombatStatType, T> item)
        {
            ((IDictionary<CombatStatType, T>)Stats).Add(item);
        }

        public void Clear()
        {
            ((IDictionary<CombatStatType, T>)Stats).Clear();
        }

        public bool Contains(KeyValuePair<CombatStatType, T> item)
        {
            return ((IDictionary<CombatStatType, T>)Stats).Contains(item);
        }

        public void CopyTo(KeyValuePair<CombatStatType, T>[] array, int arrayIndex)
        {
            ((IDictionary<CombatStatType, T>)Stats).CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<CombatStatType, T> item)
        {
            return ((IDictionary<CombatStatType, T>)Stats).Remove(item);
        }

        public IEnumerator<KeyValuePair<CombatStatType, T>> GetEnumerator()
        {
            return ((IDictionary<CombatStatType, T>)Stats).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary<CombatStatType, T>)Stats).GetEnumerator();
        }

        #endregion
    }
}