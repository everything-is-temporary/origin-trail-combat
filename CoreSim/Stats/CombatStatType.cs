namespace CombatSim.CoreSim
{
    public enum CombatStatType
    {
        Health,
        Attack,
        Defense,
        Speed,
        Luck
    }
}