using System;
using System.Threading.Tasks;

namespace CombatSim.CoreSim.Controller
{
    public class FunctionActionCommand : IActorController
    {

        private Func<CombatGame, CombatState, Task<ActionCommand>> _controllingFunction;

        public FunctionActionCommand(Func<CombatGame, CombatState, Task<ActionCommand>> controllingFunction)
        {
            _controllingFunction = controllingFunction;
        }
        
        public ActionCommand MakeMove(CombatGame combatGame, CombatState combatState)
        {
            return _controllingFunction.Invoke(combatGame, combatState).Result;
        }
    }
}