using CombatSim.CoreSim.History;

namespace CombatSim.CoreSim.Controller
{
    public interface IActorController
    {
        ActionCommand MakeMove(CombatGame combatGame, CombatState combatState);
    }
}