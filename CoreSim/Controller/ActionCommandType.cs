namespace CombatSim.CoreSim.Controller
{
    public enum ActionCommandType
    {
        None,
        Item,
        Flee
    }
}