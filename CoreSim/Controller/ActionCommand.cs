using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using CombatSim.CoreSim.Effects;

namespace CombatSim.CoreSim.Controller
{
    public class ActionCommand
    {

        public static readonly ActionCommand None = new ActionCommand(ActionCommandType.None);
        public static readonly ActionCommand Flee = new ActionCommand(ActionCommandType.Flee);

        public ActionCommandType ActionCommandType { get; protected set; }
        
        public CombatItem CombatItem { get; protected set; }

        public List<CombatActor> SelectedTargets { get; protected set; } = new List<CombatActor>();

        public readonly List<CombatActor> PossibleTargets;

        public ActionCommand(ActionCommandType actionCommandType)
        {
            ActionCommandType = actionCommandType;
        }

        public ActionCommand(CombatItem combatItem)
        {
            ActionCommandType = ActionCommandType.Item;
            CombatItem = combatItem;
        }
        
        public ActionCommand(CombatItem combatItem, List<CombatActor> possibleTargets)
        {
            ActionCommandType = ActionCommandType.Item;
            CombatItem = combatItem;
            PossibleTargets = possibleTargets;
        }

        public void SetActionCommandType(ActionCommandType actionCommandType)
        {
            ActionCommandType = actionCommandType;
        }

        public bool AddTarget(CombatActor targetToAdd)
        {
            if (!SelectedTargets.Contains(targetToAdd))
            {
                SelectedTargets.Add(targetToAdd);
                return true;
            }
            return false;
        }

        public bool RemoveTarget(CombatActor targetToRemove)
        {
            return SelectedTargets.Remove(targetToRemove);
        }

        public bool Validate(CombatState state)
        {
            CombatActor usingActor = state.ActiveCombatActor;
            if (ActionCommandType == ActionCommandType.Item)
            {
                if (CombatItem == null)
                {
                    return false;
                }

                if (SelectedTargets.Count != CombatItem.NumTargets)
                {
                    return false;
                }

                foreach (CombatActor target in SelectedTargets)
                {
                    TargetType targetTypeToActiveActor = target.TargetFor(usingActor);
                    if (!CombatItem.TargetType.ContainsAll(targetTypeToActiveActor) ||
                        !target.CanBeTargetedBy.ContainsAll(targetTypeToActiveActor) ||
                        !usingActor.CanTarget.ContainsAll(targetTypeToActiveActor))
                    {
                        {
                            return false;
                        }
                    }

                }
                return true;
            }

            return true;
        }
    }
}