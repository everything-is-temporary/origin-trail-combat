using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CombatSim.CoreSim.Controller;
using CombatSim.CoreSim.Effects;
using CombatSim.CoreSim.History;
using CombatSim.Utility;
using CombatSim.Utility.Enumerations;

namespace CombatSim.CoreSim
{
    public class CombatGame
    {

        public int RunGame(CombatState state)
        {
            state.History.Clear();
            state.History.Add(new CombatEvent(CombatEventType.StartCombat));
            while (!IsEnded(state))
            {
                state = state.Copy(); // Do not directly use a state copy as argument since any changes to it are lost; assign first.
                if (!RunTurn(state))
                {
                    break;
                }
            }

            state.History.Add(new CombatEvent(CombatEventType.EndCombat));
            Console.WriteLine("Final State:");
            Console.WriteLine(state);
            return GetWinner(state);
        }

        public bool RunTurn(CombatState state)
        {
            if (state.TurnOrder.Count == 0)
            {
                ApplyEffectsAndCountdowns(StateTimingType.GameTurnEnd, state);
                state.TurnNumber++;
                state.History.Add(new CombatEvent(CombatEventType.ChangeTurn, state.TurnNumber));
                SetTurnOrder(state);
            }
            Console.WriteLine(state);

            if (!RunNextAction(state) || IsEnded(state))
            {
                return false;
            }
            
            ApplyEffectsAndCountdowns(StateTimingType.CharacterTurnEnd, state, new List<CombatActor>{state.ActiveCombatActor});

            if (state.TurnOrder.Count > 0)
            {
                state.TurnOrder.Dequeue();
            }

            return true;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <returns>True if combat continues, false otherwise.</returns>
        public bool RunNextAction(CombatState state)
        {
            if (state.TurnOrder.Count > 0)
            {
                CombatActor activeActor = state.GetActorFromIndex(state.TurnOrder.Peek());
                ApplyEffectsAndCountdowns(StateTimingType.CharacterTurnStart, state,
                    new List<CombatActor> {activeActor});
                ActionCommand command;
                do
                {
                    // Pass a copy in case the MakeMove API modifies the state object
                    command = activeActor.Controller.MakeMove(this, state.Copy());
                } while (!ValidateCommand(state, command));

                if (!HandleCommand(state, command))
                {
                    return false;
                }
                
                ApplyEffectsAndCountdowns(StateTimingType.CharacterTurnEnd, state,
                    new List<CombatActor> {activeActor});
            }

            return true;
        }



        #region HandleControllerInput

        public bool ValidateCommand(CombatState state, ActionCommand command)
        {
            return command.Validate(state);
        }

        public bool HandleCommand(CombatState state, ActionCommand command)
        {
            if (command.ActionCommandType == ActionCommandType.Flee)
            {     
                state.History.Add(new CombatEvent(CombatEventType.AttemptedFleeBattle, state.ActiveCombatActor));
                return false;
            }

            if (command.ActionCommandType == ActionCommandType.None)
            {
                state.History.Add(new CombatEvent(CombatEventType.NoActionMade, state.ActiveCombatActor));
                return true;
            }
            
            if (command.ActionCommandType == ActionCommandType.Item)
            {
                state.History.Add(new CombatEvent(CombatEventType.ItemUsed, (state.ActiveCombatActor, command.CombatItem, command.SelectedTargets)));

                state.ActiveCombatActor.DecrementItem(command.CombatItem);
                foreach (CombatActor target in command.SelectedTargets)
                {
                    var damage = CalculateItemDamage(state.ActiveCombatActor, target, command.CombatItem);
                    if (damage > 0)
                    {
                        target.TakeDamage(damage);
                        state.History.Add(new CombatEvent(CombatEventType.DamageTaken, (target, damage)));
                        if (!target.CanFight)
                        {
                            target.ExitActiveCombat();
                        }
                    }

                    foreach (CombatEffect effect in command.CombatItem.Effect)
                    {
                        if (effect.EffectDuration == EffectDurationType.Momentary)
                        {
                            effect.Apply(state, target);
                            state.History.Add(new CombatEvent(CombatEventType.MomentaryEffectApplied, (target, effect)));
                        }
                        else
                        {
                            target.AddEffect(effect);
                        }
                    }
                }
            }

            return true;
        }

        #endregion
        
        public int GetWinner(CombatState state)
        {
            return state.GetActorsInActiveCombat.Select(actor => actor.GroupId).Distinct().Count() == 1
                ? state.GetActorsInActiveCombat.First().GroupId
                : -1;
        }
        
        public bool IsEnded(CombatState state)
        {
            return GetWinner(state) != -1;
        }
        
        public CombatState GetInitialState(List<CombatActor> combatActors)
        {
            CombatState cs = new CombatState();
            foreach (CombatActor actor in combatActors)
            {
                cs.AddActor(actor);
                cs.History.Add(new CombatEvent(CombatEventType.ActorEnterCombat, actor));
            }
            return cs;
        }
        
        public void SetTurnOrder(CombatState state)
        {
            var order = state.GetActorsInActiveCombat.OrderBy(actor => actor.GetStat(CombatStatType.Speed))
                .Select(actor => state.GetIndexFromActor(actor)).ToList();
            state.TurnOrder = new Queue<int>(order);
            state.History.Add(new CombatEvent(CombatEventType.ChangeTurnOrder, order));
        }

        public void ApplyEffectsAndCountdowns(StateTimingType timingType, CombatState state, List<CombatActor> effectsFor = null)
        {
            if (effectsFor == null)
            {
                effectsFor = state.AllActors;
            }

            foreach (CombatActor combatActor in effectsFor)
            {
                combatActor.ApplyAll(timingType, state);
            }
        }
        
        #region DamageCalculation

        public double CalculateItemDamage(CombatActor source, CombatActor target, CombatItem item)
        {
            if (item.StatsActual.Keys.Count == 0)
            {
                // an item without stats deals no damage
                return 0;
            }
            CombatStatType maxStat = item.StatsActual.Where(pair => pair.Key != CombatStatType.Health).OrderByDescending(pair => pair.Value).First().Key;
            double aggregateAttack = source.GetStat(maxStat) + item.StatsActual[maxStat];
            double netDamage = aggregateAttack * GetPositionalMultiplier(source) - target.GetStat(CombatStatType.Defense) / GetPositionalMultiplier(target);
            var q = ApplyLuckDamageModifier(netDamage, source, target, item);
            return ApplyLuckDamageModifier(netDamage, source, target, item);
        }

        private static double variationMultiplier = 1.0;
        private double ApplyLuckDamageModifier(double damage, CombatActor source, CombatActor target, CombatItem item)
        {
            //Variation (What percent of damage can be maximally added or subtracted from damage?)
            double denom = 2 * target.GetStat(CombatStatType.Luck) + item.StatsActual[CombatStatType.Luck];
            double variation = variationMultiplier * ((target.GetStat(CombatStatType.Luck) - item.StatsActual[CombatStatType.Luck]) / (denom == 0 ? .5 : denom)) + 1;
            
            // Value from 0 to 1, 0.5 when stats are equal TODO: Maybe use Gaussian sampling for a normal distribution?
            double adjustment = 0.5*(source.GetStat(CombatStatType.Luck) - target.GetStat(CombatStatType.Luck)) /
                                (source.GetStat(CombatStatType.Luck) + target.GetStat(CombatStatType.Luck)) + 0.5;
            return  (rng.NextDouble() + adjustment - 0.5) * variation * damage + damage;
        }

        #region PositionAdjustment
                
        private double GetPositionalMultiplier(CombatActor target)
        {
            // Between 1 and 2 (only 1 for non-affected)
            return target.CanReposition ? 1 + target.Position : 1;
        }

        public static double maxAdjustement = 0.2;
        private void ApplyPoitionalChange(double damageDealt, ref CombatActor damageReciever)
        {
            if (damageReciever.CanReposition)
            {
                damageReciever.Position =
                    MathUtils.Clamp(1 - damageDealt / damageReciever.GetStat(CombatStatType.Health), damageReciever.Position - maxAdjustement, damageReciever.Position + maxAdjustement);
            }
        }
        
        #endregion
        
        #endregion
        
        public List<ActionCommand> GetAllMoves(CombatState state)
        {
            List<ActionCommand> commands = new List<ActionCommand>{ActionCommand.Flee, ActionCommand.None};
            foreach (CombatItem combatItem in state.ActiveCombatActor.Items)
            {
                var possibleTargets = GetValidTargets(state, state.ActiveCombatActor, combatItem);
                if (possibleTargets.Count > 0)
                {
                    commands.Add(new ActionCommand(combatItem, possibleTargets));
                }
            }

            return commands;
        }
        
        public List<CombatActor> GetValidTargets(CombatState state, CombatActor source, TargetType targetType)
        {
            // TODO: NOTE: Very important, if two actors are the same instance, TargetFor() will return self, even for enemies
            return state.GetActorsInActiveCombat
                .Select(actor => new KeyValuePair<CombatActor, TargetType>(actor, source.TargetFor(actor))).Where(kvp =>
                    kvp.Value.ContainsAny(targetType) && source.CanTarget.ContainsAll(kvp.Value) &&
                    kvp.Key.CanBeTargetedBy.ContainsAny(targetType)).Select(kvp => kvp.Key).ToList();
        }

        public List<CombatActor> GetValidTargets(CombatState state, CombatActor source, CombatItem combatItem)
        {
            return GetValidTargets(state, source, combatItem.TargetType);
        }

        private static Random rng = new Random();
    }
}